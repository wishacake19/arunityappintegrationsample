package com.android.arintegrationsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.wishacake.cakebottom.UnityPlayerActivity;


public class MainActivity extends AppCompatActivity {

    private Button mButtonOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonOpen = (Button) findViewById(R.id.btn_open);
        mButtonOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, UnityPlayerActivity.class);
                startActivity(intent);
                Toast.makeText(MainActivity.this, "Successful.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
